#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d"

ENVS=(
  POSTFIX_HOSTNAME
  POSTFIX_MYSQL_HOST
  POSTFIX_MYSQL_USER
  POSTFIX_MYSQL_PASS
  POSTFIX_MYSQL_DB
  POSTFIX_DEFAULT_TRANSPORT
  POSTFIX_DOVECOT_LMTP_TRANSPORT
  POSTFIX_MESSAGE_SIZE_LIMIT
  POSTFIX_SMTP_SECURITY_LEVEL
)

# cleanup ENV variables
for e in "${ENVS[@]}"; do
  unset "$e"
done

# run
exec "$@"
