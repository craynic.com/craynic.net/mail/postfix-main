#!/usr/bin/env bash

set -Eeuo pipefail

CONFD_DIR="/opt/postfix/conf.d/"
CONFIG_FILE="/etc/postfix/main.cf"
BACKUP_SUFFIX="bak"
DOCKER_INIT_DIR="/docker-init.d/"

inotifywait-dir.sh "$CONFD_DIR" | while read -r line; do
  echo "$line"

  # restore configuration file from backup
  cp -a "$CONFIG_FILE.$BACKUP_SUFFIX" "$CONFIG_FILE"

  find "$DOCKER_INIT_DIR" -type f | sort -n | while read -r SCRIPT_FILENAME; do
    PREFIX=$(basename "$SCRIPT_FILENAME" | cut -d"-" -f1);

    if [[ $PREFIX -gt 50 ]]; then
      echo "Running $SCRIPT_FILENAME"

      # shellcheck disable=SC1090
      . "$SCRIPT_FILENAME"
    fi
  done

  # reload postfix
  /usr/local/sbin/reload-postfix.sh
done
