#!/usr/bin/env bash

set -Eeuo pipefail

for FILENAME in "$@"; do
  cat -- "$FILENAME" \
  | sed ':a;N;$!ba;s/\r\?\n[[:space:]]\+/ /g' \
  | ( grep -v "^#" || true ) \
  | while read -r LINE || [[ -n "$LINE" ]]; do
    postconf -e "$LINE"
  done
done

