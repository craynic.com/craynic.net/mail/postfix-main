#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_DOVECOT_LMTP_TRANSPORT" ]]; then
  echo "Missing configuration for Dovecot's LMTP endpoint" >/dev/stderr
  exit 1
fi

if ! [[ "$POSTFIX_DOVECOT_LMTP_TRANSPORT" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The Doveoct LMTP endpoint does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the server address. Are you sure you want that?"
fi

update-postfix-config.sh <(echo "virtual_transport = lmtp:inet:$POSTFIX_DOVECOT_LMTP_TRANSPORT")

# make the TLS encryption mandatory
postconf -P "lmtp/unix/smtp_tls_security_level=encrypt"
# enable XFORWARD
postconf -P "lmtp/unix/lmtp_send_xforward_command=yes"
