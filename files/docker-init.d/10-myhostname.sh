#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_HOSTNAME" ]]; then
  postconf -X "myhostname"
else
  update-postfix-config.sh <(echo "myhostname = $POSTFIX_HOSTNAME")
fi
