#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$POSTFIX_DEFAULT_TRANSPORT" ]]; then
  echo "Missing address for default_transport" >/dev/stderr
  exit 1
fi

if ! [[ "$POSTFIX_DEFAULT_TRANSPORT" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The default_transport address does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the server address. Are you sure you want that?"
fi

update-postfix-config.sh <(echo "default_transport = smtp:$POSTFIX_DEFAULT_TRANSPORT")

# make the TLS encryption mandatory
postconf -P "smtp/unix/smtp_tls_security_level=encrypt"
# enable XFORWARD
postconf -P "smtp/unix/smtp_send_xforward_command=yes"
